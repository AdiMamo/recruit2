@extends('layouts.app')
@section('title', 'Interviwes')
@section('content')

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Candidate</th><th>User</th><th>Summary</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>

            <td>
                @if(isset($interview->candidate))
                    {{$interview->candidate->name}}
                @endif

            </td>
            <td>
                @if(isset($interview->user))
                    {{$interview->user->name}}
                @endif
            </td>
            <td>{{$interview->summary}}</td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>   
        </tr>
    @endforeach
</table>
@endsection

