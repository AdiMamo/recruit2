@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf
        <div class="form-group">
            <label for = "name">Date</label>
            <input type = "text" class="form-control" name = "date" placeholder="input the date of interview">
        </div>     
        <div class="form-group">
            <label for = "email">Summary</label>
            <input type = "text" class="form-control" name = "summary" placeholder="input the summary of interview">
        </div> 

        <div class="form-group row">
                            <label for="candidate_id" class="col-md-2 col-form-label text-md-right">Candidate</label>
                            <div class="col-md-6">
                                <select class="form-control" name="candidate_id">                                                                         
                                   @foreach ($candidates as $candidate)
                                     <option value="{{ $candidate->id }}"> 
                                         {{ $candidate->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_id" class="col-md-2 col-form-label text-md-right">User</label>
                            <div class="col-md-6">

                                <select class="form-control" name="user_id">                                                                         
                                   @foreach ($users as $user)

                                     <option value="{{ $user->id }}"> 
                                         {{ $user->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>



        <div>
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection
