<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020-07-14',
                'summary' => 'accepted to work',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020-07-15',
                'summary' => 'not accepted to work',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);            
    }
}
